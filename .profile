# disable core file generation
ulimit -c 0

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games:.

export PATH=$PATH:$HOME/bin:$HOME/bin/ascii
export EDITOR="/usr/local/bin/nvim"
export VISUAL=$EDITOR
# export BROWSER="/usr/local/bin/tabbed -c /usr/local/bin/surf -e"
export BROWSER="/usr/local/bin/surf"
if [ "$(uname)" == "Linux" ] ; then
    export TERM="st-256color"
else # OpenBSD
    export TERM="st-git-256color"
    # set to max power performance
    sudo apmd
    sudo apm -H
fi

export LOCALE='en_US.UTF-8'
export LC_CTYPE='en_US.UTF-8'
export LESSCHARSET='utf-8'

if test -n "$KSH_VERSION"; then
    ENV=~/.kshrc ; . $ENV
    export ENV
elif test -n "$ZSH_VERSION"; then
    source .zshrc
elif test -n "$BASH_VERSION"; then
    source .bashrc
fi

if test xdg-open ; then
    export XDG_DESKTOP_DIR="$HOME"
    export XDG_CONFIG_HOME="$HOME/.config"
    export XDG_DOCUMENTS_DIR="$HOME/files"
    export XDG_DOWNLOAD_DIR="$HOME/downloads"
    export XDG_MUSIC_DIR="$HOME/music"
    export XDG_PICTURES_DIR="$HOME/images"
    export XDG_VIDEOS_DIR="$HOME/videos"
    export XDG_PUBLICSHARE_DIR="/non/existent"
fi

if [ $(pgrep X) ] ; then
    echo "X has already been started."
else
    startx
fi
