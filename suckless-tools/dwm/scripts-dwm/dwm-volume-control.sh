#!/bin/dash

case $1 in
    --raise) $(dash ${HOME}/bin/BSDNixVolume.sh -set "+$2") ;;
    --lower) $(dash ${HOME}/bin/BSDNixVolume.sh -set "-$2") ;;
    --mute) $(dash ${HOME}/bin/BSDNixVolume.sh -mute) ;;
esac
 
val=$(dash ${HOME}/bin/BSDNixVolume.sh -get)
if [ $val -gt 48 ] ; then 
    echo "\\uf028%{T3} $val%%{T-}" > /tmp/bar/vol
elif [ $val -gt 0 ] ; then
    echo "\\uf027%{T3} $val%%{T-}" > /tmp/bar/vol
else 
    echo "\\uf026" > /tmp/bar/vol
fi
