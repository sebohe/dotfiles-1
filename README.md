# dotfiles
my personal dotfiles

Also see my ~/bin: http://github.com/MitchWeaver/bin

Note: I switch between Linux and BSD, but no worries, you'll find most of my scripts supporting both via $(uname) checks.


screenshots:

![Image](screenshots/sc6.png)

![Image](screenshots/sc0.png)

![Image](screenshots/sc5.png)

![Image](screenshots/sc1.png)

![Image](screenshots/sc2.png)

![Image](screenshots/sc3.png)

![Image](screenshots/sc4.png)

Feel free to fork.

Sharing is caring! If you make something cool you think I would like to know about, please pull request!


If you like my designs, check out some of my projects.
