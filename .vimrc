" https://github.com/MitchWeaver/dotfiles/.vimrc
"
"         ________ ++     ________
"       /VVVVVVVV\++++  /VVVVVVVV\
"       \VVVVVVVV/++++++\VVVVVVVV/
"        |VVVVVV|++++++++/VVVVV/'
"        |VVVVVV|++++++/VVVVV/'
"       +|VVVVVV|++++/VVVVV/'+
"     +++|VVVVVV|++/VVVVV/'+++++
"   +++++|VVVVVV|/VVV___++++++++++
"     +++|VVVVVVVVVV/##/ +_+_+_+_
"       +|VVVVVVVVV___ +/#_#,#_#,\
"        |VVVVVVV//##/+/#/+/#/'/#/
"        |VVVVV/'+/#/+/#/+/#/ /#/
"        |VVV/'++/#/+/#/ /#/ /#/
"        'V/'  /##//##//##//###/
"                ++

" unbind space for everything but leader
nnoremap <silent><SPACE> <nop>
let mapleader=" "
" ←--------------- Plugins -----------------------------------→
set nocompatible
filetype off
call plug#begin('~/.vim/vim-plug')

" --- Applications
Plug 'vimwiki/vimwiki'
Plug 'ctrlpvim/ctrlp.vim' " fuzzy finder
" Plug 'iamcco/markdown-preview.vim' " doesn't work in neovim

" --- Themes & Frippery
" Plug 'flazz/vim-colorschemes' " just a bunch of colorschemes
" Plug 'altercation/vim-colors-solarized' " solarized colorschemes
" Plug 'jonathanfilip/vim-lucius' " lucius colorscheme
" Plug 'arcticicestudio/nord-vim' " nord colorscheme
Plug 'dylanaraps/wal.vim'

" --- Syntax Highlighting
" Plug 'sheerun/vim-polyglot' " syntax highlight - all languages
" Plug 'pangloss/vim-javascript' " better javascript support
Plug 'lilydjwg/colorizer' " colorizes rgb colorcodes

" --- Code Completion
Plug 'ervandew/supertab' " open code completion with tab
" Plug 'Shougo/deoplete.nvim' " parallelized code completion (neovim) SLOW!

" --- Utils
Plug 'tpope/vim-commentary' " comment toggler - You can't live without this.
Plug 'terryma/vim-multiple-cursors' " sublime multiple select
Plug 'tpope/vim-surround' " quote/paren etc surrounding
Plug 'airblade/vim-gitgutter' " git diffing along the left side
Plug 'godlygeek/tabular' " tab alignment
Plug 'AndrewRadev/splitjoin.vim' " conversion from multiline to singleline
" Plug 'w0rp/ale' " asynchronous linting in many languages (neovim) SLOW!
Plug 'terryma/vim-smooth-scroll'

call plug#end()
filetype indent plugin on
syntax enable
map <silent><leader>pi :PlugInstall<CR>
map <silent><leader>pu :PlugUpdate<CR>
map <silent><leader>pc :PlugClean<CR>
map <silent><leader>pa :PlugClean<CR>:PlugInstall<CR>:PlugUpdate<CR>
" --------------------------------------------------------------

" ------------- COLORSCHEME ------------------------------------
" colorscheme newspaper
" colorscheme lucius
" LuciusDarkHighContrast
" LuciusDarkLowContrast
" LuciusLightHighContrast
colorscheme wal

set background=dark
" set background=light
" hi Normal ctermbg=NONE " disables background:
" ---------------------------------------------------------------

" -------------- Vim Specific Configs -------------------------
set mouse=a " NOTE THIS BREAKS MIDDLE CLICK PASTE on some terminals
" make backspace useable
set backspace=indent,eol,start
set whichwrap+=<,>,h,l
set updatetime=750 " how long til vim does background calls after typing
set timeout! " Disable keybind timeout
set ttimeout! " Disable keybind timeout
set clipboard=unnamed " yank/paste to/from system clipboard
set vb " disable audible bell
set novisualbell " kill the visual bell too
set noerrorbells " did I mention I hate bells?
set lazyredraw " whether to redraw screen after macros
set mat=2 " how fast to blink matched brackets
set ch=1 " command height --- get rid of the wasted line at the bottom
set textwidth=0 " very annoying warning occurs with long lines
set backspace=2 " allow backspace to go over new lines, etc
set ttimeoutlen=100 " timeout between key presses for cmds
" -------------------------------------------------------------

" ---------------- OS Specific Configs ------------------------
set shellslash
set shell=ksh
set wildmenu " makes shell completion a bit better
set wildmode=longest,list,full
set ffs=unix,dos,mac
" -------------------------------------------------------------

" ------------ HISTORY ---------------------------------------
set history=750
set undolevels=500
set undoreload=2000
" -----------------------------------------------------------

" ---------- UI ---------------------------------------------
" highlight trailing whitespace - (annoying imo)
" highlight ExtraWhitespace ctermbg=darkgreen guibg=darkgreen
" match ExtraWhitespace /\s\+$\|\t/

set formatoptions+=o " continue comment marker on new lines

set noruler " don't show where you are in the bottom right
set noshowmode " don't show 'insert' or 'normal' etc on bottom left
set laststatus=0 " Disable bottom status line / statusbar
set noshowcmd

" ~~~ I prefer these two disabled at startup, see function below
" set number " enables line numbers on startup
" set relativenumber " shows line numbers relative to position

" Toggle line numbering on/off
map <silent><leader>ln :set number! relativenumber!<cr>

set showmatch " show matching parens

set hid " hide buffer when closed

" WARNING - this is *very* slow, I have it disabled
" set cursorline " show current line

" Nice, but this can be annoying on some color schemes
" set colorcolumn=80

set scrolloff=10 " keep cursor X lines from the top/bottom when scrolling
set mousehide " hide the mouse while typing (doesn't work on openbsd?)

set fillchars=""  " extremely annoying, they serve no purpose

" don't syntax highlight lines that are too long (slow)
set synmaxcol=1024
" ------------------------------------------------------------

" ------ FORMATTING -----------------------------------------
set nowrap " dont wrap lines
set encoding=utf-8
set diffopt+=iwhite " disable white space diffing
" --------------------------------------------------------------

" -------- Tabs and Spacing -----------------------------------
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab " use spaces instead of tabs.
set smarttab " let's tab key insert 'tab stops', and bksp deletes tabs.
set shiftround " tab / shifting moves to closest tabstop.
set autoindent " match indents on new lines.
set smartindent " intellegently dedent / indent new lines based on rules
" --------------------------------------------------------------

" ---------- File Management -------------------------------------
set nobackup " we have vcs, we don't need backups.
set nowritebackup " we have vcs, we don't need backups.
set noswapfile " they're just annoying. who likes them?
set hidden " allow me to have buffers with unsaved changes.
set autoread " when a file has changed on disk, just load it. don't ask.
" -------------------------------------------------------------

" -------- Tabs ----------------------------------------------
set tabpagemax=10 " dont show more than 10 tabs
map <silent><C-h>  :tabfirst<CR>
map <silent><C-k>  :tabnext<CR>
map <silent><C-j>  :tabprev<CR>
map <silent><C-l>  :tablast<CR>

map <silent>tt  :tabedit<Space>
map <silent>tn  :tabnew<CR>
map <silent>tm  :tabm<Space>
map <silent>td  :tabclose<CR>
" ------------------------------------------------------------

" ---------- Movement -------------------------------------------
"  scroll up/down without moving the cursor position
nnoremap <silent><C-e> <C-e>
nnoremap <silent><C-w> <C-y>
map <silent><C-y> <nop>
" -----------------------------------------------------------------

" ---------- Searching ----------------------------------------
set ignorecase " case insensitive search
set smartcase " if there are uppercase letters, become case-sensitive.
set incsearch " live incremental searching
set showmatch " live match highlighting
set hlsearch " highlight matches
set gdefault " use the `g` flag by default.
set wrapscan " searching wraps lines
set magic " 'magic' patterns - (extended regex)
" Clear the highlighted search -- note: does not disable, only clears.
nnoremap <silent><leader><leader> :let @/ = ""<CR>

" Search and Replace
nmap <Leader>s :%s//g<Left><Left>
" -------------------------------------------------------------

" -------- Language Syntax Management ---------------------------
iab #i #include <.h>
let g:is_ksh=1 " vim's default sh syntax highlighting is horrible
" --------------------------------------------------------------

" -------------- Extension Settings --------------------------
" disable all gitgutter keybinds
let g:gitgutter_map_keys = 0
" only run gitgutter on save
let g:gitgutter_realtime = 0
let g:gitgutter_eager = 0

" toggle gitgutter
map <silent><leader>g :GitGutterToggle<CR>

let g:multi_cursor_use_default_mapping=0
" i use 'm' here because c-n is reserved for `tabbed`
let g:multi_cursor_next_key='<c-m>'
let g:multi_cursor_prev_key='<c-p>'
let g:multi_cursor_skip_key='<c-x>'
let g:multi_cursor_quit_key='<esc>'

" map the key for toggling comments with vim-commentary
nmap <silent><leader>c :Commentary<CR>

" ---------- Ale settings ----------------------------
"  ~~~~ i only use this for a rare few languages
"       it's too slow to be useful in others ~~~~~
" only lint on file save, not in background or on file open:
" let g:ale_lint_on_text_changed = 'never'
" let g:ale_lint_on_enter = 0
" Disable warnings about trailing whitespace for Python files.
" let b:ale_warn_about_trailing_whitespace = 0 " fucking annoying

" ------------ Vim Wiki ---------------------------------------
let wiki = {}
let g:vimwikidir = "/home/mitch/files/vimwiki"
let wiki.path = g:vimwikidir
let g:vimwiki_list=[wiki]
let g:vimwiki_hl_headers = 1
let g:vimwiki_hl_cb_checked = 1
let g:vimwiki_list = [
    \{'path': '~/files/vimwiki/personal.wiki',    'syntax': 'markdown', 'ext': '.md'},
\]
let g:vimwiki_ext2syntax = {'.md': 'markdown', '.markdown': 'markdown', '.mdown': 'markdown'}
map <silent><leader>md :MarkdownPreview<CR>

" ctrlp ignores
set wildignore+=/home/mitch/music,/home/mitch/videos,/home/mitch/books,/home/mitch/images,*.opus,*.flac,*.pdf,*.jpg,*.png,*.so,*.swp,*.zip,*.gzip,*.bz2,*.tar,*.xz,*.lrzip,*.lrz,*.mp3,*.ogg,*.mp4,*.gif,*.jpeg,*.webm
" --------------------------------------------------------------

" ────────────────── Vim Surround ──────────────────────── "
" ## todo:
" ──────────────────────────────────────────────────────── "

" ------ misc keybinds -------------------------------------------
"  Make shift-insert work (better if you can just make your terminal do it)
" map <silent><S-Insert> <MiddleMouse>
" map! <silent><S-Insert> <MiddleMouse>

map <F8> :setlocal spell! spelllang=en_gb<CR>

" Horizontal scrolling
nmap <silent> <C-o> 10zl
nmap <silent> <C-i> 10zh

" Nuke +, -, ! at start of lines in diffs (also killing the - lines)
map <silent> <C-z> :%s/^+<CR> :%s/^-.*<CR> :%s/^!<CR>

" Clear CtrlP cache
map <silent> <F5> :CtrlPClearCache<CR>

" saving and exiting
map <silent><leader>w :w<CR>
map <silent><leader>q :q<CR>
" ----------------------------------------------------------------

" ----------------- Symbol Printing -----------------------------
" ~~~ NOTE: I know you can do these with default vim, ctrl-k
" conflicts with my own bindings. Plus, I think this way is easier.

" ~~~~~~ Greek Symbols ~~~~~ "
" lambda
imap <silent><C-q><C-l> λ
" theta
imap <silent><C-q><C-t> Θ
" delta
imap <silent><C-q><C-d> Δ
" pi
imap <silent><C-q><C-p> π
" mu
imap <silent><C-q><C-u> μ
" omega
imap <silent><C-q><C-o> Ω
" phi
imap <silent><C-q><C-[> Φ
" kappa
imap <silent><C-q><C-k> κ
" sigma
imap <silent><C-q><C-s> ∑

" ~~~~ Logical Symbols ~~~~~ "
" bullet / dot operator
imap <silent><C-q><C-b> ∙
" therefore
imap <silent><C-q><C-.> ∴
" because
imap <silent><C-q><C-,> ∵
" member of
imap <silent><C-q><C-3> ∋
" element of
imap <silent><C-q><C-e> ∈
" Null set
imap <silent><C-q><C-0> ∅
" for all
imap <silent><C-q><C-a> ∀
" right arrow
imap <silent><C-q><C-Right> →
" left arrow
imap <silent><C-q><C-Left> ←
" up arrow
imap <silent><C-q><C-Up> ↑
" down arrow
imap <silent><C-q><C-Down> ↓
" there exists
imap <silent><C-q><C-E> ∃

" ~~~~~~ Mathematics ~~~~~~~ "
" sqrt
imap <silent><C-q><C-s><C-q> √
" infinity
imap <silent><C-q><C-i> ∞
" squared
imap <silent><C-q><C-2> ²
" plus/minus
imap <silent><C-q><C-p> ±
" roughly equivalent
imap <silent><C-q><C-q> ≈
" not equal to
imap <silent><C-q><C-/> ≠
" greater than/equal to
imap <silent><C-q><C-g> ≥
" less than/equal to
imap <silent><C-q><C-l> ≤

" ~~~~~~~~ Misc ~~~~~~~~~~~~ "
" trademark symbol
imap <silent><C-q><C-t><C-m> ™
" copyright symbol
imap <silent><C-q><C-c><C-r> ©
" left guillemet
imap <silent><C-q><C-]> »
" right guillemet
imap <silent><C-q><C-[> «

" Things I don't use but might be cool:
" (calculus) integrals: ∫∬∮

" print an 80-char line separator
inoremap <silent><C-s> # ─────────────────────────────────────────────────────── #<ESC>0llllllllllllllllllllli
inoremap <silent><C-z> # ─────────────────────────────────────────────────────────────────────────── #<ESC>0lllllllllllllllllllllo
" --------------------------------------------------------------

" ----------- Page Up/Down Functionality ----------------------
"  I know ctrl-f/b do the same, but I like having both.
function GetNumberOfVisibleLines()
  let cur_line = line(".")
  let cur_col = virtcol(".")
  normal H
  let top_line = line(".")
  normal L
  let bot_line = line(".")
  execute "normal " . cur_line . "G"
  execute "normal " . cur_col . "|"
  return bot_line - top_line
endfunc

function! PageUp()
  let visible_lines = GetNumberOfVisibleLines()
  execute "normal " . visible_lines . "\<C-U><silent>:set scroll=0\r"
endfunction

function! PageDown()
  let visible_lines = GetNumberOfVisibleLines()
  execute "normal " . visible_lines . "\<C-D><silent>:set scroll=0\r"
endfunction

" noremap <silent><PageUp> :call PageUp()<CR>
" noremap <silent><PageDown> :call PageDown()<CR>
" I switch the f/b keys, and also add them to insert mode.
" noremap <silent><C-f> :call PageUp()<CR>
" noremap <silent><C-b> :call PageDown()<CR>
" imap <silent><C-f> <ESC> :call PageUp()<CR><i>
" imap <silent><C-b> <ESC> :call PageDown()<CR><i>

" When using the vim-smooth-scroll plugin:
noremap <silent><C-u> :call smooth_scroll#up(&scroll, 0, 2)<CR>
noremap <silent><C-d> :call smooth_scroll#down(&scroll, 0, 2)<CR>
noremap <silent><C-b> :call smooth_scroll#down(&scroll*2, 0, 4)<CR>
noremap <silent><C-f> :call smooth_scroll#up(&scroll*2, 0, 4)<CR>
noremap <silent><PageUp> :call smooth_scroll#up(&scroll*2, 0, 4)<CR>
noremap <silent><PageDown> :call smooth_scroll#down(&scroll*2, 0, 4)<CR>
" ---------------------------------------------------------------

" ------------- Unbindings ---------------------------------------
" disable Arrow keys in normal mode
nmap  <silent><up>     <nop>
nmap  <silent><down>   <nop>
nmap  <silent><left>   <nop>
nmap  <silent><right>  <nop>
" disable Arrow keys in insert mode
imap <silent><up>    <nop>
imap <silent><down>  <nop>
imap <silent><right> <nop>
imap <silent><left>  <nop>
" disable binds that conflict with st
map  <silent><c-=>    <nop>
map  <silent><c-->    <nop>
map  <silent><c-l>    <nop>
" disable binds that conflict with tabbed
map  <silent><C-n>    <nop>
map  <silent><C-w>    <nop>
" --------------------------------------------------------------------------

" ------------ autocmds -----------------------------------------------
" Delete trailing white space on save
func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc
autocmd BufWrite *.py :call DeleteTrailingWS()
autocmd BufWrite *.hs :call DeleteTrailingWS()
autocmd BufWrite *.coffee :call DeleteTrailingWS()
" --------------------------------------------------------------------------

" ------------ Open files in ranger (nvim only) --------------------------------- "
function! s:RangerOpenDir(...)
	let path = a:0 ? a:1 : getcwd()

	if !isdirectory(path)
		echom 'Not a directory: ' . path
		return
	endif

	let s:ranger_tempfile = tempname()
	let opts = ' --cmd="set viewmode multipane"'
	let opts .= ' --choosefiles=' . shellescape(s:ranger_tempfile)
	if a:0 > 1
		let opts .= ' --selectfile='. shellescape(a:2)
	else
		let opts .= ' ' . shellescape(path)
	endif
	let rangerCallback = {}

	function! rangerCallback.on_exit(id, code, _event)
		" Open previous buffer or new buffer *before* deleting the terminal 
		" buffer. This ensures that splits don't break if ranger is opened in 
		" a split window.
		if w:_ranger_del_buf != w:_ranger_prev_buf
			" Restore previous buffer
			exec 'silent! buffer! '. w:_ranger_prev_buf
		else
			" Previous buffer was empty
			enew
		endif

		" Delete terminal buffer
		exec 'silent! bdelete! ' . w:_ranger_del_buf

		unlet! w:_ranger_prev_buf w:_ranger_del_buf

		let names = ''
		if filereadable(s:ranger_tempfile)
			let names = readfile(s:ranger_tempfile)
		endif
		if empty(names)
			return
		endif
		for name in names
			exec 'edit ' . fnameescape(name)
			doautocmd BufRead
		endfor
	endfunction

	" Store previous buffer number and the terminal buffer number
	let w:_ranger_prev_buf = bufnr('%')
	enew
	let w:_ranger_del_buf = bufnr('%')

	" Open ranger in nvim terminal
	call termopen('ranger ' . opts, rangerCallback)
	startinsert
endfunction

" Override netrw
let g:loaded_netrwPlugin = 'disable'
augroup ReplaceNetrwWithRanger
	autocmd StdinReadPre * let s:std_in = 1
	autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | call s:RangerOpenDir(argv()[0]) | endif
augroup END

command! -complete=dir -nargs=* Ranger :call <SID>RangerOpenDir(<f-args>)
" command! -complete=dir -nargs=* RangerCurrentFile :call <SID>RangerOpenDir(expand('%:p:h'), @%)
nnoremap <silent><leader>r :Ranger<CR>
" --------------------------------------------------------------------------- "
